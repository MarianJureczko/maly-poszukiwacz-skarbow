package pl.marianjureczko.poszukiwacz.activity.treasureselector

import java.io.Serializable

data class Coordinates(val latitude: Double, val longitude: Double) : Serializable
